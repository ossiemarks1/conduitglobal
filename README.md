# conduitglobal

Ansible CX100 deployment with autoscaling

# AWS instance creation with post provisioning

**Current CentOS Version Used**: 7.5

This repo can be used to create an:

  - AWS CX100 stack with auto-scaling groups

The example can be modified to use more Ansible roles, plays, and included playbooks to fully configure (or partially) configure a box file suitable for deployment for development environments.

## Requirements

The following software must be installed/present on your local machine before you can use Packer to build the Vagrant box file:
  - ansible
  - boto
  - ec2.py

If you don't have Ansible installed (perhaps you're using a Windows PC?), you can simply clone the required Ansible roles from GitHub directly (use [Ansible Galaxy](https://galaxy.ansible.com/) to get the GitHub repository URLs for each role listed in `requirements.txt`), and update the `role_paths` variable to match the location of the cloned role.

## Usage

Make sure all the required software (listed above) is installed, then cd to the directory containing this README.md file, and run:

    $ export AWS_SECRET_ACCESS_KEY=blahblahblah
    $ export AWS_SECRET_ACCESS_KEY=blahblahblah

    $ ansible-playbook -i ansible/inventory/ec2.py ansible/main.yml --extra-vars=the_state=absent to remove the stack ( if you omit state, it defaults to present)
For the AMI version then please run the following:

TODO:

add auto populate etc/hosts for all nodes but most importantly for the console server for connectign to the other servers.
add log aggregator to capture logs from all nodes in a central log store or in an s3 bucket or cloud watch.
